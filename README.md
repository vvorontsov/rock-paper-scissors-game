# Rock-Paper-Scissors game
---
##To run this web application do the next:
- clone this repository: git clone https://vvorontsov@bitbucket.org/vvorontsov/rock-paper-scissors-game.git
- create ***mysql*** database named "rock_paper_scissors", add user "admin" with password "supersecurepwd". Example of script:
```sql
SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema rock_paper_scissors
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `rock_paper_scissors` ;
CREATE SCHEMA `rock_paper_scissors` DEFAULT CHARACTER SET utf8 ;
USE `rock_paper_scissors` ;

DROP USER IF EXISTS'admin'@'localhost';
CREATE USER 'admin'@'localhost' IDENTIFIED BY 'supersecurepwd';
GRANT ALL PRIVILEGES ON * . * TO 'admin'@'localhost';
FLUSH PRIVILEGES;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
```
- do: ***mvn clean package***;
- go to ***liquibase*** plugin and run command "update";
- deploy project file to Tomcat and run it;
- browse this application in your browser.
- credentials: 
  - username: **admin**
  - password: **admin**
- then you can choose mode of game: single or multiply;
- in the **multiply** mode you can choose shape selection mode for 2 players, count of games and simulate the game specified times. Then you will get a count of player A and player B wins and count of ties;
**For example**, you want to simulate **100 games** when player A always choose **only scissors** and player's B choice is **random**. So, choose player A mode: "Choose scissors only", player B mode: "Random choice" and enter count of games = 100. Them push the "Start mutiply game" button and see all results.
- in the **single** mode you can choose shape of hand and play the game. Shape of your enemy generated automatically. Then you will get the result of game. Furthermore, by pushing "Statistics" button you can see the history of your wins, losses and ties in the single game mode.