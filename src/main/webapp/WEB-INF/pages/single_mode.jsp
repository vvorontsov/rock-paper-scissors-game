<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Single game</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style.css">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
    <sec:authorize access="hasRole('ROLE_USER')">
    <c:if test="${(results == true) or (stat == true)}">
        <script type="text/javascript" src="${pageContext.request.contextPath}/jQuery.js"></script>
    </c:if>
    </sec:authorize>
</head>
<body>
<h1 class="title">Rock-Paper-Scissors</h1>
<sec:authorize access="hasRole('ROLE_USER')">
<div id="body-box">
<form action="${pageContext.request.contextPath}/single_mode" method="get">
    <label> Choose your shape:<br>
        <div class="gold">
        <input type="radio" name="shape" value=0 checked> Rock<br>
        <input type="radio" name="shape" value=1> Paper<br>
        <input type="radio" name="shape" value=2> Scissors<br>
        <input type="hidden" name="resultRequest" value="true">
        </div>
        <input type="submit" value="Play!">
    </label>
</form>
    <form action="${pageContext.request.contextPath}/single_mode" method="get">
        <input type="hidden" name="statistic" value="true">
        <input type="submit" value="Statistics">
    </form>
</div>
<c:if test="${(results == true) or (stat == true)}">
    <div id="boxes">
        <div id="dialog" class="window">
            <div class="top"><a href="#" class="link close">Close</a></div>
            <div class="content">
                <c:if test="${results == true}">
                <c:choose>
                    <c:when test="${game_result == 1}">
                        You win!
                    </c:when>
                    <c:when test="${game_result == -1}">
                        You lose :(
                    </c:when>
                    <c:when test="${game_result == 0}">
                        It's a tie.
                    </c:when>
                </c:choose><br>
                    Your choice: ${player_shape}<br>
                    Enemy's choice: ${generated_shape}
                </c:if>
                <c:if test="${stat == true}">
                    Wins: ${user_wins}<br>
                    Losses: ${user_losses}<br>
                    Ties: ${user_ties}<br>
                    Total count of games: ${user_total}
                </c:if><br>
            </div>
        </div>
    </div>
    <div id="mask"></div>
</c:if>
<a href="${pageContext.request.contextPath}/game_selection">Back</a>
<jsp:include page="logout_footer.jsp"/>
</sec:authorize>
</body>
</html>