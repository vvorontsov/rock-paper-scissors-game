<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Selection</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style.css">
</head>
<body>
<h1 class="title">Rock-Paper-Scissors</h1>
<sec:authorize access="hasRole('ROLE_USER')">
    <div id="body-box" style="font-size: 35px">
    <a href="${pageContext.request.contextPath}/single_mode">Single game mode</a><br><br>
    <a href="${pageContext.request.contextPath}/multiply_mode">Multiply game mode</a><br>
    </div>
    <jsp:include page="logout_footer.jsp"/>
</sec:authorize>
</body>
</html>
