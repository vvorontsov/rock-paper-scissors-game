<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<body>
<!-- For login user -->
<form action="${pageContext.request.contextPath}/logout" method="post"
      id="logoutForm">
    <input type="hidden" name="${_csrf.parameterName}"
           value="${_csrf.token}"/>
</form>
<script>
    function formSubmit() {
        document.getElementById("logoutForm").submit();
    }
</script>

<c:if test="${pageContext.request.userPrincipal.name != null}">
    <h2 class="user">
        User : ${pageContext.request.userPrincipal.name} | <a href="javascript:formSubmit()"> Logout</a>
    </h2>
</c:if>
</body>
</html>
