<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page session="true" %>
<html>
<head>
    <title>Login Page</title>
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/style.css">
</head>
<body onload='document.loginForm.username.focus();'>

<h1 class="title">Rock-Paper-Scissors</h1>

<div id="login-box">

    <h2 class="simple">Login</h2>

    <c:choose>
        <c:when test="${pageContext.request.userPrincipal.name == null}">
            <c:if test="${not empty error}">
                <div class="error">${error}</div>
            </c:if>
            <c:if test="${not empty msg}">
                <div class="msg">${msg}</div>
            </c:if>
            <form name="loginForm"
                  action="${pageContext.request.contextPath}/login"
                  method="POST">
                <table>
                    <tr>
                        <td class="user">User:</td>
                        <td><input type="text" name="username"></td>
                    </tr>
                    <tr>
                        <td class="user">Password:</td>
                        <td><input type="password" name="password"/></td>
                    </tr>
                    <tr>
                        <td colspan="2"><input name="submit" type="submit"
                                               value="submit"/></td>
                    </tr>
                </table>

                <input type="hidden" name="${_csrf.parameterName}"
                       value="${_csrf.token}"/>

            </form>
        </c:when>
        <c:otherwise>
            <div class="msg">
                You are already logged in
            </div>
            <jsp:include page="logout_footer.jsp"/>
        </c:otherwise>
    </c:choose>
</div>

</body>
</html>