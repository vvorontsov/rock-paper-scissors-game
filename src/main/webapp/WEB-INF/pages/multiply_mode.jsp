<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Multiply mode</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style.css">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
    <sec:authorize access="hasRole('ROLE_USER')">
    <c:if test="${results == true}">
        <script type="text/javascript" src="${pageContext.request.contextPath}/jQuery.js"></script>
    </c:if>
    </sec:authorize>
<body>
<h1 class="title">Rock-Paper-Scissors</h1>
<sec:authorize access="hasRole('ROLE_USER')">
<c:if test="${results == true}">
    <div id="boxes">
        <div id="dialog" class="window">
            <div class="top"><a href="#" class="link close">Close</a></div>
            <div class="content">
                Player A wins ${player_A_wins} of ${count_of_games}<br>
                Player B wins ${player_B_wins} of ${count_of_games}<br>
                Tie: ${tie} of ${count_of_games}<br>
            </div>
        </div>
    </div>
    <div id="mask"></div>
</c:if>
    <div id="body-box">
    <form action="${pageContext.request.contextPath}/multiply_mode" method="get">
    Enter count of games:<br>
    <c:choose>
        <c:when test="${not empty err}">
            <input type="number" name="count_of_games" value="100" class="err_bound">
        </c:when>
        <c:otherwise>
            <input type="number" name="count_of_games" value="100">
        </c:otherwise>
    </c:choose><br><br>
    <div class="gold">Select Player A mode:</div>
    <select name="player_A_mode">
        <option value=0>Choose only Rock</option>
        <option value=1>Choose only Paper</option>
        <option value=2 selected>Choose only Scissors</option>
        <option value=3>Random choice</option>
    </select><br><br>
    <div class="gold">Select Player B mode:</div>
    <select name="player_B_mode">
        <option value=0>Choose only Rock</option>
        <option value=1>Choose only Paper</option>
        <option value=2>Choose only Scissors</option>
        <option value=3 selected>Random choice</option>
    </select><br>
    <input type="hidden" name="playRequest" value="true"/>
    <input type=submit style="margin: 10px 10px 10px 10px" value="Start multiply game">
    </form>
    </div>
<c:if test="${not empty err}">
    <div class="error">${err}</div>
</c:if>
<a href="${pageContext.request.contextPath}/game_selection">Back</a
<jsp:include page="logout_footer.jsp"/>
</sec:authorize>
</body>
</html>
