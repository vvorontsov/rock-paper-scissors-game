package com.pearsonfrank.testtask.controller;

import com.pearsonfrank.testtask.config.ApplicationContextConfig;
import com.pearsonfrank.testtask.core.basic.dto.MultiplyTimeGameResults;
import com.pearsonfrank.testtask.entity.User;
import com.pearsonfrank.testtask.service.abstraction.GameRunner;
import com.pearsonfrank.testtask.service.abstraction.UserServiceFunctionality;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.NoSuchElementException;

/**
 * Main controller between model and view.
 */
@Controller
@PropertySource("classpath:message.properties")
public class MainController {

    private final Environment env;

    @Autowired
    private GameRunner gameRunner;

    @Autowired
    private ApplicationContextConfig.IAuthenticationFacade auth;

    @Autowired
    private UserServiceFunctionality userService;

    @Autowired
    public MainController(Environment env) {
        this.env = env;
    }

    /**
     * Method directs visitor of web site to login page.
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView home() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("login_page");
        return modelAndView;
    }

    /**
     * @param error  in the case of login error.
     * @param logout in the case of successful logout.
     * @return login page.
     */
    @RequestMapping(value = "/login_page", method = RequestMethod.GET)
    public ModelAndView login(@RequestParam(value = "error", required = false) String error,
                              @RequestParam(value = "logout", required = false) String logout) {
        ModelAndView model = new ModelAndView();
        if (error != null) {
            model.addObject("error", env.getProperty("mes.err.login"));
        }
        if (logout != null) {
            model.addObject("msg", env.getProperty("mes.logout"));
        }
        model.setViewName("login_page");
        return model;
    }

    /**
     * User will see this page if he will try to access some page which he
     * isn't able to access through his rights.
     * It isn't used right now. Can be useful at future.
     *
     * @return error message.
     */
    @RequestMapping(value = "/403", method = RequestMethod.GET)
    public ModelAndView accessDenied() {
        ModelAndView model = new ModelAndView();
        //check if user is login
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (!(auth instanceof AnonymousAuthenticationToken)) {
            UserDetails userDetail = (UserDetails) auth.getPrincipal();
            model.addObject("username", userDetail.getUsername());
        }
        model.setViewName("403");
        return model;
    }

    /**
     * This method shows page with game mode selection.
     */
    @RequestMapping(value = "/game_selection", method = RequestMethod.GET)
    public ModelAndView selectionPage() {
        ModelAndView model = new ModelAndView();
        model.setViewName("game_selection");
        return model;
    }

    /**
     * This method shows multiply game mode page and result of multiply game if it's required.
     *
     * @param getResults   request to get results of the game.
     * @param aPlayerMode  shape selection mode of player A.
     * @param bPlayerMode  shape selection mode of player B.
     * @param countOfGames total count of games.
     */
    @RequestMapping(value = "/multiply_mode", method = RequestMethod.GET)
    public ModelAndView multiplyGameMode(
            @RequestParam(value = "playRequest", required = false, defaultValue = "false") boolean getResults,
            @RequestParam(value = "player_A_mode", required = false) Integer aPlayerMode,
            @RequestParam(value = "player_B_mode", required = false) Integer bPlayerMode,
            @RequestParam(value = "count_of_games", required = false) Integer countOfGames) {
        ModelAndView model = new ModelAndView();
        model.addObject("results", false);
        if (getResults) {
            if (countOfGames == null) {
                model.addObject("err", env.getProperty("mes.err.empty.count"));
            } else {
                MultiplyTimeGameResults results;
                try {
                    results = gameRunner.playMultiplyGame(aPlayerMode, bPlayerMode, countOfGames);
                } catch (NoSuchElementException ex) {
                    model.setViewName("403");
                    return model;
                }
                model.addObject("results", true);
                model.addObject("player_A_wins", results.getWins());
                model.addObject("player_B_wins", results.getLosses());
                model.addObject("tie", results.getTie());
                model.addObject("count_of_games", countOfGames);
            }
        }
        model.setViewName("multiply_mode");
        return model;
    }

    /**
     * This method shows page of single game mode and dialog window
     * with results of single game or statistics if required.
     *
     * @param shape         shape of hand selected by player.
     * @param resultRequest request to get result of game.
     * @param statistic     request to get statistics of games.
     * @return single game mode page.
     */
    @RequestMapping(value = "/single_mode", method = RequestMethod.GET)
    public ModelAndView playSingleGame(
            @RequestParam(value = "shape", required = false) Integer shape,
            @RequestParam(value = "resultRequest", required = false, defaultValue = "false") boolean resultRequest,
            @RequestParam(value = "statistic", required = false, defaultValue = "false") boolean statistic) {
        ModelAndView model = new ModelAndView();
        model.addObject("results", false);
        model.addObject("stat", false);
        if (resultRequest) {
            model.addObject("results", true);
            Integer generatedShape = gameRunner.generateRandomShape();
            Integer result = gameRunner.playSingleGame(shape, generatedShape);
            model.addObject("game_result", result);
            model.addObject("player_shape", gameRunner.getNameOfShape(shape));
            model.addObject("generated_shape", gameRunner.getNameOfShape(generatedShape));
            userService.updateStatistic(result, auth.getAuthentication().getName());
        }
        if (statistic) {
            model.addObject("stat", true);
            User authUser = userService.findUser(auth.getAuthentication().getName());
            model.addObject("user_wins", authUser.getUserWins());
            model.addObject("user_losses", authUser.getUserLosses());
            model.addObject("user_ties", authUser.getUserTies());
            model.addObject("user_total", authUser.getUserTotal());
        }
        model.setViewName("single_mode");
        return model;
    }
}