package com.pearsonfrank.testtask.service.abstraction;

import com.pearsonfrank.testtask.core.basic.dto.MultiplyTimeGameResults;

public interface GameRunner {

    /**
     * This method should simulate the series of games. Allowed mods of game:
     * 0 - always choose Rock;
     * 1 - always choose Paper;
     * 2 - always choose Scissors;
     * 3 - random choice every iteration.
     *
     * @param firstPlayerMode  - game mode of first player.
     * @param secondPlayerMode - game mode of second player.
     * @param countOfGames     - total count of games.
     * @return count of wins of first player, wins of second player and ties.
     */
    MultiplyTimeGameResults playMultiplyGame(Integer firstPlayerMode, Integer secondPlayerMode, Integer countOfGames);

    /**
     * This method should implement game between the real player and computer. Allowed choices:
     * 0 - choose rock;
     * 1 - choose paper;
     * 2 - choose scissors.
     * @param playerChoice - shape chosen by player
     * @param computerChoice - shape chosen by computer
     * @return -1 if player looses, 1 if player wins, 0 if there is a tie.
     */
    Integer playSingleGame(Integer playerChoice, Integer computerChoice);

    /**
     * This method should generate random choice. Allowed choices:
     * 0 - choose rock;
     * 1 - choose paper;
     * 2 - choose scissors.
     * @return id of shape.
     */
    Integer generateRandomShape();

    /**
     * This method converts id of shape to appropriate shape name.
     * @param shapeId id of shape.
     * @return name of shape.
     */
    String getNameOfShape(Integer shapeId);
}
