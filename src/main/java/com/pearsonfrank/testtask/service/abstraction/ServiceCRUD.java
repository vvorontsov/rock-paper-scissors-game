package com.pearsonfrank.testtask.service.abstraction;

import java.util.List;

/**
 * Implementation should contain realization of CRUD operations:
 * - create
 * - update
 * - read
 * - delete
 * Each method should contain appropriate script for database.w
 * @param <E> entity class
 */
public interface ServiceCRUD<E> {
    void insert(E entity);

    List<E> selectAll();

    E selectById(Integer id);

    void update(E entity);

    void delete(E entity);
}
