package com.pearsonfrank.testtask.service;

import com.pearsonfrank.testtask.core.basic.ShapeOfHand;
import com.pearsonfrank.testtask.dao.element.functionality.UserFunctionality;
import com.pearsonfrank.testtask.entity.User;
import com.pearsonfrank.testtask.service.abstraction.UserServiceFunctionality;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class UserService implements UserServiceFunctionality {

    @Autowired
    private UserFunctionality userDAO;

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void insert(User entity) {
        userDAO.addElement(entity, sessionFactory);
    }

    @Override
    public List<User> selectAll() {
        return userDAO.getAllElements(sessionFactory);
    }

    @Override
    public User selectById(Integer id) {
        return userDAO.getElementByID(id, sessionFactory);
    }

    @Override
    public void update(User entity) {
        userDAO.updateElement(entity, sessionFactory);
    }

    @Override
    public void delete(User entity) {
        userDAO.deleteElement(entity, sessionFactory);
    }

    @Override
    public User findUser(String username) {
        return userDAO.findUser(username, sessionFactory);
    }

    @Override
    public void updateStatistic(Integer result, String userName) {
        User user = findUser(userName);
        switch (result) {
            case ShapeOfHand.LOSS:
                user.setUserLosses(user.getUserLosses() + 1);
                break;
            case ShapeOfHand.TIE:
                user.setUserTies(user.getUserTies() + 1);
                break;
            case ShapeOfHand.WINS:
                user.setUserWins(user.getUserWins() + 1);
                break;
        }
        user.setUserTotal(user.getUserTotal() + 1);
        update(user);
    }
}
