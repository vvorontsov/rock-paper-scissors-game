package com.pearsonfrank.testtask.service;

import com.pearsonfrank.testtask.core.basic.dto.MultiplyTimeGameResults;
import com.pearsonfrank.testtask.core.game.MultiplyTimesGameCore;
import com.pearsonfrank.testtask.core.game.SingleGameCore;
import com.pearsonfrank.testtask.core.game.inter.MultiplyTimesGame;
import com.pearsonfrank.testtask.core.game.inter.SingleGame;
import com.pearsonfrank.testtask.service.abstraction.GameRunner;
import org.springframework.stereotype.Service;

@Service
public class GameRunnerService implements GameRunner {

    private static final MultiplyTimesGame multiplyGameCore = new MultiplyTimesGameCore();
    private static final SingleGame singleGameCore = new SingleGameCore();

    @Override
    public MultiplyTimeGameResults playMultiplyGame(Integer firstPlayerMode, Integer secondPlayerMode, Integer countOfGames) {
        return multiplyGameCore.playTheMultiplyGame(firstPlayerMode, secondPlayerMode, countOfGames);
    }

    @Override
    public Integer playSingleGame(Integer playerChoice, Integer computerChoise) {
        return singleGameCore.playTheGame(playerChoice, computerChoise);
    }

    @Override
    public Integer generateRandomShape() {
        return singleGameCore.generateRandomShape();
    }

    @Override
    public String getNameOfShape(Integer shapeId) {
        return singleGameCore.getNameOfShape(shapeId);
    }
}
