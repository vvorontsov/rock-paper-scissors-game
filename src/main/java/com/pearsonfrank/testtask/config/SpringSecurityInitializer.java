package com.pearsonfrank.testtask.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Spring security initializer.
 */
public class SpringSecurityInitializer extends AbstractSecurityWebApplicationInitializer {

    // Do nothing

}