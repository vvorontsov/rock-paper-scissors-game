package com.pearsonfrank.testtask.core.basic;

/**
 * this enum contains all available shapes (rock, paper or scissors) in the game
 */
public enum ShapeOfHand{
    ROCK(0) {
        public int wins(ShapeOfHand shape) {
            if (shape == ShapeOfHand.PAPER) {
                return LOSS;
            } else if (shape == ShapeOfHand.SCISSORS) {
                return WINS;
            }
            return TIE;
        }
    },
    PAPER(1) {
        public int wins(ShapeOfHand shape) {
            if (shape == ShapeOfHand.SCISSORS) {
                return LOSS;
            } else if (shape == ShapeOfHand.ROCK) {
                return WINS;
            }
            return TIE;
        }
    },
    SCISSORS(2) {
        public int wins(ShapeOfHand shape) {
            if (shape == ShapeOfHand.ROCK) {
                return LOSS;
            } else if (shape == ShapeOfHand.PAPER) {
                return WINS;
            }
            return TIE;
        }
    };

    private final Integer id;
    public static final int WINS = 1;
    public static final int LOSS = -1;
    public static final int TIE = 0;

    ShapeOfHand(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public abstract int wins(ShapeOfHand shape);
}