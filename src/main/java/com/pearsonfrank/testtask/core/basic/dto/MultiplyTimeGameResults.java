package com.pearsonfrank.testtask.core.basic.dto;

public class MultiplyTimeGameResults {
    private Integer wins = 0;
    private Integer losses = 0;
    private Integer tie = 0;

    public MultiplyTimeGameResults(Integer wins, Integer losses, Integer tie) {
        this.wins = wins;
        this.losses = losses;
        this.tie = tie;
    }

    public Integer getWins() {
        return wins;
    }

    public void setWins(Integer wins) {
        this.wins = wins;
    }

    public Integer getLosses() {
        return losses;
    }

    public void setLosses(Integer losses) {
        this.losses = losses;
    }

    public Integer getTie() {
        return tie;
    }

    public void setTie(Integer tie) {
        this.tie = tie;
    }
}
