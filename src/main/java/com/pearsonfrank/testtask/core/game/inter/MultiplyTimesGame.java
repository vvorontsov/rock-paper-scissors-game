package com.pearsonfrank.testtask.core.game.inter;

import com.pearsonfrank.testtask.core.basic.dto.MultiplyTimeGameResults;

public interface MultiplyTimesGame {
    int ALWAYS_CHOOSE_ROCK = 0;
    int ALWAYS_CHOOSE_PAPER = 1;
    int ALWAYS_CHOOSE_SCISSORS = 2;
    int RANDOM_CHOICE = 3;

    MultiplyTimeGameResults playTheMultiplyGame(Integer firstPlayerMode,
                                                Integer secondPlayerMode,
                                                Integer countOfGames);
}
