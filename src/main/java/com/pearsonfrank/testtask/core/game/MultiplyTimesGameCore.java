package com.pearsonfrank.testtask.core.game;

import com.pearsonfrank.testtask.core.basic.ShapeOfHand;
import com.pearsonfrank.testtask.core.basic.dto.MultiplyTimeGameResults;
import com.pearsonfrank.testtask.core.game.inter.MultiplyTimesGame;

import java.util.NoSuchElementException;

/**
 * implementation of multiply game mode core
 */
public class MultiplyTimesGameCore implements MultiplyTimesGame {

    private static final SingleGameCore singleCore = new SingleGameCore();

    public MultiplyTimeGameResults playTheMultiplyGame(Integer firstPlayerMode,
                                                       Integer secondPlayerMode,
                                                       Integer countOfGames) {
        Integer temp = 0;
        Integer ties = 0;
        for (int i = 0; i < countOfGames; i++) {
            Integer firstPlayerChoice = getPlayerChoice(firstPlayerMode);
            Integer secondPlayerChoice = getPlayerChoice(secondPlayerMode);
            Integer result = singleCore.playTheGame(firstPlayerChoice, secondPlayerChoice);
            temp += result;
            if (result.equals(ShapeOfHand.TIE)) {
                ties++;
            }
        }
        /*
        let x - count of wins, y - count of losses. Then we have a system:
        x - y = temp
        x + y = countOfGames - ties
        After solving this system, we have x = (countOfGames +  temp - ties) / 2
         */
        Integer wins = (countOfGames + temp - ties) / 2;
        Integer losses = countOfGames - wins - ties;
        return new MultiplyTimeGameResults(wins, losses, ties);
    }

    private Integer getPlayerChoice(Integer playerMode) {
        switch (playerMode) {
            case ALWAYS_CHOOSE_ROCK:
                return ShapeOfHand.ROCK.getId();
            case ALWAYS_CHOOSE_PAPER:
                return ShapeOfHand.PAPER.getId();
            case ALWAYS_CHOOSE_SCISSORS:
                return ShapeOfHand.SCISSORS.getId();
            case RANDOM_CHOICE:
                return singleCore.generateRandomShape();
        }
        throw new NoSuchElementException("Specified player mode (id = " + playerMode + ") doesn't exist");
    }
}
