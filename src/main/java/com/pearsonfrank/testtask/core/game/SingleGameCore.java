package com.pearsonfrank.testtask.core.game;

import com.pearsonfrank.testtask.core.basic.ShapeOfHand;
import com.pearsonfrank.testtask.core.game.inter.SingleGame;

import java.util.NoSuchElementException;
import java.util.concurrent.ThreadLocalRandom;

/**
 * implementation of single game mode core
 */
public class SingleGameCore implements SingleGame {
    public Integer playTheGame(Integer firstPlayerChoice, Integer
            secondPlayerChoice) {
        ShapeOfHand player = getShapeOfHand(firstPlayerChoice);
        ShapeOfHand enemy = getShapeOfHand(secondPlayerChoice);
        return player.wins(enemy);
    }

    public Integer generateRandomShape() {
        return ThreadLocalRandom.current().nextInt(0, 3);
    }

    @Override
    public String getNameOfShape(Integer shapeId) {
        for (ShapeOfHand shape: ShapeOfHand.values()) {
            if (shape.getId().equals(shapeId)) {
                return shape.toString();
            }
        }
        return "Undefined shape";
    }

    private static ShapeOfHand getShapeOfHand(Integer playersChoice) {
        if (playersChoice.equals(ShapeOfHand.ROCK.getId())) {
            return ShapeOfHand.ROCK;
        }
        if (playersChoice.equals(ShapeOfHand.PAPER.getId())) {
            return ShapeOfHand.PAPER;
        }
        if (playersChoice.equals(ShapeOfHand.SCISSORS.getId())) {
            return ShapeOfHand.SCISSORS;
        }
        throw new NoSuchElementException("Can't find enum with specified id = " + playersChoice);
    }
}
