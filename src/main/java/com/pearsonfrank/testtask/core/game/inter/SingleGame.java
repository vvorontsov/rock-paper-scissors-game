package com.pearsonfrank.testtask.core.game.inter;

public interface SingleGame {

    Integer playTheGame(Integer firstPlayerChoice, Integer secondPlayerChoice);

    Integer generateRandomShape();

    String getNameOfShape(Integer shapeId);
}
