package com.pearsonfrank.testtask.dao;

import com.pearsonfrank.testtask.dao.element.UserDAO;

/**
 * Class provides singletons of each DAO.
 */
public class SingletonDAO {

    private static UserDAO userDAO;
    private SingletonDAO() {}

    public static UserDAO getUserDAO() {
        if (userDAO == null) {
            userDAO = new UserDAO();
        }
        return userDAO;
    }
}
