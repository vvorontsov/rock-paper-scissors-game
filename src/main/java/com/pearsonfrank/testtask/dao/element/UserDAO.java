package com.pearsonfrank.testtask.dao.element;

import com.pearsonfrank.testtask.dao.ElementDAO;
import com.pearsonfrank.testtask.dao.element.functionality.UserFunctionality;
import com.pearsonfrank.testtask.entity.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

@Repository
@Transactional
public class UserDAO extends ElementDAO<User> implements UserFunctionality {

    public UserDAO() {
        super(User.class);
    }

    @Override
    public User findUser(String username, SessionFactory sessionFactory) {
        Session session = sessionFactory.openSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<User> crit = builder.createQuery(User.class);
        Root<User> acc = crit.from(User.class);
        crit.where(builder.equal(acc.get("userName"), username));
        TypedQuery<User> q = session.createQuery(crit);
        return q.getSingleResult();
    }
}
