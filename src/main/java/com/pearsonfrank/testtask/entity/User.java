package com.pearsonfrank.testtask.entity;

import javax.persistence.*;

/**
 * Entity class for user
 */
@Entity
@Table(name = "user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private Integer userId;

    @Column(name = "user_name")
    private String userName;

    @Column(name = "user_password")
    private String userPassword;

    @Column(name = "user_wins")
    private Integer userWins;

    @Column(name = "user_losses")
    private Integer userLosses;

    @Column(name = "user_ties")
    private Integer userTies;

    @Column(name = "user_total")
    private Integer userTotal;

    public User() {}

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public Integer getUserWins() {
        return userWins;
    }

    public void setUserWins(Integer userWins) {
        this.userWins = userWins;
    }

    public Integer getUserLosses() {
        return userLosses;
    }

    public void setUserLosses(Integer userLosses) {
        this.userLosses = userLosses;
    }

    public Integer getUserTies() {
        return userTies;
    }

    public void setUserTies(Integer userTies) {
        this.userTies = userTies;
    }

    public Integer getUserTotal() {
        return userTotal;
    }

    public void setUserTotal(Integer userTotal) {
        this.userTotal = userTotal;
    }
}
