package core;

import com.pearsonfrank.testtask.core.basic.dto.MultiplyTimeGameResults;
import com.pearsonfrank.testtask.core.game.MultiplyTimesGameCore;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.NoSuchElementException;

public class MultiplyTimesGameTest {

    private static final int COUNT_OF_TEST_GAMES = 100;
    private static final MultiplyTimesGameCore core = new MultiplyTimesGameCore();

    @Test
    public void playTheMultiplyGame_onlyWins() {
        Integer firstPlayer = MultiplyTimesGameCore.ALWAYS_CHOOSE_PAPER;
        Integer secondPlayer = MultiplyTimesGameCore.ALWAYS_CHOOSE_ROCK;

        MultiplyTimeGameResults res = core.playTheMultiplyGame(firstPlayer, secondPlayer, COUNT_OF_TEST_GAMES);

        Assert.assertEquals(res.getWins().intValue(), COUNT_OF_TEST_GAMES);
        Assert.assertEquals(res.getLosses().intValue(), 0);
        Assert.assertEquals(res.getTie().intValue(), 0);
    }

    @Test
    public void playTheMultiplyGame_onlyLosses() {
        Integer firstPlayer = MultiplyTimesGameCore.ALWAYS_CHOOSE_SCISSORS;
        Integer secondPlayer = MultiplyTimesGameCore.ALWAYS_CHOOSE_ROCK;

        MultiplyTimeGameResults res = core.playTheMultiplyGame(firstPlayer, secondPlayer, COUNT_OF_TEST_GAMES);

        Assert.assertEquals(res.getWins().intValue(), 0);
        Assert.assertEquals(res.getLosses().intValue(), COUNT_OF_TEST_GAMES);
        Assert.assertEquals(res.getTie().intValue(), 0);
    }

    @Test
    public void playTheMultiplyGame_onlyTies() {
        Integer firstPlayer = MultiplyTimesGameCore.ALWAYS_CHOOSE_ROCK;
        Integer secondPlayer = MultiplyTimesGameCore.ALWAYS_CHOOSE_ROCK;

        MultiplyTimeGameResults res = core.playTheMultiplyGame(firstPlayer, secondPlayer, COUNT_OF_TEST_GAMES);

        Assert.assertEquals(res.getWins().intValue(), 0);
        Assert.assertEquals(res.getWins().intValue(), 0);
        Assert.assertEquals(res.getTie().intValue(), COUNT_OF_TEST_GAMES);
    }

    @Test(expectedExceptions = NoSuchElementException.class)
    public void playMultiplyGame_wrongPlayerMode() {
        core.playTheMultiplyGame(4, 4, COUNT_OF_TEST_GAMES);
    }
}
