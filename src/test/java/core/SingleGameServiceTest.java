package core;

import com.pearsonfrank.testtask.core.basic.ShapeOfHand;
import com.pearsonfrank.testtask.core.game.SingleGameCore;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.NoSuchElementException;

public class SingleGameServiceTest {

    private static final int ROCK = ShapeOfHand.ROCK.getId();
    private static final int PAPER = ShapeOfHand.PAPER.getId();
    private static final int SCISSORS = ShapeOfHand.SCISSORS.getId();
    private static final SingleGameCore core = new SingleGameCore();

    @Test
    public void playTheGame_rockBeatsScissors() {
        int resultOfGame = core.playTheGame(ROCK, SCISSORS);

        Assert.assertEquals(resultOfGame, ShapeOfHand.WINS);
    }

    @Test
    public void playTheGame_rockLosesPaper() {
        int resultOfGame = core.playTheGame(ROCK, PAPER);

        Assert.assertEquals(resultOfGame, ShapeOfHand.LOSS);
    }

    @Test
    public void playTheGame_rockTie() {
        int resultOfGame = core.playTheGame(ROCK, ROCK);

        Assert.assertEquals(resultOfGame, ShapeOfHand.TIE);
    }

    @Test
    public void playTheGame_paperBeatsRock() {
        int resultOfGame = core.playTheGame(PAPER, ROCK);

        Assert.assertEquals(resultOfGame, ShapeOfHand.WINS);
    }

    @Test
    public void playTheGame_paperLosesScissors() {
        int resultOfGame = core.playTheGame(PAPER, SCISSORS);

        Assert.assertEquals(resultOfGame, ShapeOfHand.LOSS);
    }

    @Test
    public void playTheGame_paperTie() {
        int resultOfGame = core.playTheGame(PAPER, PAPER);

        Assert.assertEquals(resultOfGame, ShapeOfHand.TIE);
    }

    @Test
    public void playTheGame_scissorsBeatsPaper() {
        int resultOfGame = core.playTheGame(SCISSORS, PAPER);

        Assert.assertEquals(resultOfGame, ShapeOfHand.WINS);
    }

    @Test
    public void playTheGame_scissorsLosesRock() {
        int resultOfGame = core.playTheGame(SCISSORS, ROCK);

        Assert.assertEquals(resultOfGame, ShapeOfHand.LOSS);
    }

    @Test
    public void playTheGame_scissorsTie() {
        int resultOfGame = core.playTheGame(SCISSORS, SCISSORS);

        Assert.assertEquals(resultOfGame, ShapeOfHand.TIE);
    }

    @Test(expectedExceptions = NoSuchElementException.class)
    public void playTheGame_unsupportedShape_throwException() {
        core.playTheGame(3, 3);
    }

    @Test
    public void getNameOfShape_rock() {
        String name = core.getNameOfShape(ROCK);

        Assert.assertEquals("ROCK", name);
    }
    @Test

    public void getNameOfShape_paper() {
        String name = core.getNameOfShape(PAPER);

        Assert.assertEquals("PAPER", name);
    }

    @Test
    public void getNameOfShape_scissors() {
        String name = core.getNameOfShape(SCISSORS);

        Assert.assertEquals("SCISSORS", name);
    }
}